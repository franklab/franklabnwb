import datajoint as dj
import session

schema = dj.schema('franklab')

#define the schema for intervals
@schema
class IntervalList(dj.Manual):
    definition = """
    -> session.Session
    interval_name: varchar(80) #descriptive name of this interval list
    ---
    valid_times: longblob # 2D array with start and end times for each interval
    """