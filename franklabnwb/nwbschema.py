#Test of automatic datajoint schema generation from NWB file
import datajoint as dj

schema = dj.schema("franklab", locals())

@schema
class Nwbfile(dj.Manual):
     definition = """
    nwb_file_name: varchar(80)
    ---
    """


@schema
class Subject(dj.Manual):
    definition = """
    subject_id: varchar(80)
    ---
    age: varchar(80)
    description: varchar(80)
    genotype: varchar(80)
    sex: enum('M', 'F', 'U')
    species: varchar(80)
    """


@schema
class Device(dj.Manual):
    definition = """
    device_name: varchar(80)
    ---
    """


@schema
class Apparatus(dj.Manual):
     definition = """
    apparatus_name: varchar(80)
    ---
    -> Nwbfile
    module: varchar(80)
    container: varchar(80)
    """


@schema
class Session(dj.Manual):
     definition = """
    session_id: varchar(80)
    ---
    -> Nwbfile
    -> Subject
    -> Institution
    -> Lab
    session_description: varchar(80)
    session_start_time: datetime
    timestamps_reference_time: datetime
    experiment_description: varchar(80)
    """


@schema
class Experimenter(dj.Manual):
    definition = """
    experimenter_name: varchar(80)
    -> LabMember
    -> Session
    ---
    """


