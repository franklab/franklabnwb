# franklabnwb

Helper code for Loren Frank's lab at UCSF to work with [Neurodata Without Borders .nwb files](https://github.com/NeurodataWithoutBorders/pynwb)