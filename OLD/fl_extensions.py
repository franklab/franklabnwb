from pynwb.spec import NWBGroupSpec, NWBAttributeSpec, NWBLinkSpec, NWBNamespaceBuilder, NWBDatasetSpec, NWBDtypeSpec
from pynwb.form.spec import RefSpec
from pynwb import get_class, load_namespaces
import os
import warnings


ext_path = '/Users/loren/Src/NWB/franklabnwb/fl_extensions'
ns_name = 'franklab'
ns_path = '%s/%s.namespace.yaml' % (ext_path, ns_name)
ext_source = "%s.extensions.yaml" % ns_name

ns_builder = NWBNamespaceBuilder('Extensions for use in the the Frank Laboratory', 'franklab')


# tables and clustering structures from Andrew
FL_Cluster_Metrics_Table_ext = NWBDatasetSpec('Cluster/unit information',
							 dtype=[
								NWBDtypeSpec('id', 'the unique identifier for the cluster/unit', 'int'),
								NWBDtypeSpec('noise_overlap', 'noise overlap quality metric', 'float'),
								NWBDtypeSpec('isolation_distance', 'isolation distance quality metric', 'float')
							 ],
							 neurodata_type_def='FLClusterMetricsTable',
							 neurodata_type_inc='NWBData')

# add the class to the namespace builder
ns_builder.add_spec(ext_source, FL_Cluster_Metrics_Table_ext)


FL_SpikeEventSeries_ext = NWBGroupSpec('MountainSort spike event series',
							 groups=[
								# This can be a link if you know will always want multiple series
								# that will point to one
								NWBGroupSpec('Valid time interval for this unit',
											 name='valid_times',
											 # Setting linkable to true here so that you have the flexibility of doing both
											 linkable=True,
											 neurodata_type_inc='IntervalSeries'),
							 ],
							 datasets=[
								NWBDatasetSpec('the unit id for each spike event',
							 			   		name='unit_ids', shape=(None,),
												attributes=[ # save a reference to the table these IDs apply to
								 					NWBAttributeSpec('table', 'the ClusterTable these ids apply to',
																	 RefSpec('FLClusterMetricsTable', 'object')),
								 			   ]),
							 ],
							 neurodata_type_def='FLSpikeEventSeries',
							 neurodata_type_inc='SpikeEventSeries')


# add the class to the namespace builder
ns_builder.add_spec(ext_source, FL_SpikeEventSeries_ext)


#define a franklab ElectrodeTableRegion that includes:
#		an attribute of ntrode to correspond to the tetrode or probe shank for this region
#		a link to an IntervalSeries defining the set of times that are valid for this ntrode
# FL_ElectrodeTableRegion_ext = NWBGroupSpec('Frank Lab ElectrodeTableRegion',
#								  attributes=[NWBAttributeSpec('ntrode',
#															   'int',
#															   'The user defined number for this collection of electrodes')],
#								  links=[NWBLinkSpec('link to IntervalSeries indicating valid time range',
#													 'IntervalSeries',
#													 quantity=1,
#													 name='valid_times')],
#								  neurodata_type_inc='ElectrodeTableRegion',
#								  neurodata_type_def='FL_ElectrodeTableRegion')
#
# ns_builder.add_spec(ext_source, FL_ElectrodeTableRegion_ext)
#define a franklab ClusterQuality group that includes
#   an attribute for isolation distance from MountainSort
#	 an attribute for noise overlap from MountainSort
# QUESTION: Should we add signal to noise or mean spike amplitude?





#define a franklab SpikeUnit that includes
#	a link to the relevant SpikeEventSeries (which stores the waveforms)
#	a link to an IntervalSeries that defines the valid start and end times
#	a link to the FL_ElectrodeTableRegion that demarcates the set of channels that are part of this electrode
#	 a link to a FL_ClusterQuality object that contains information about the cluster quality for this unit

# FL_SpikeUnit_ext = NWBGroupSpec('Frank Lab Spike Unit',
# 								links=[NWBLinkSpec('link to SpikeEventSeries containing waveforms',
# 												   'SpikeEventSeries',
# 												   quantity=1,
# 												   name='spike_waveforms'),
# 									   NWBLinkSpec(
# 										   'link to IntervalSeries containing times where this unit was clustered',
# 										   'IntervalSeries',
# 										   quantity=1,
# 										   name='valid_times'),
# 									   NWBLinkSpec('link to ElectrodeTableRegion for the relevant ntrode',
# 												   'ElectrodeTableRegion',
# 												   quantity=1,
# 												   name='electrodes'),
# 									   NWBLinkSpec('link to FL_ClusterQuaity',
# 												   'FL_ClusterQuality',
# 												   quantity=1,
# 												   name='cluster_quality')],
# 								neurodata_type_inc='SpikeUnit',
# 								neurodata_type_def='FL_SpikeUnit')

#add the class to the namespace builder
#ns_builder.add_spec(ext_source, FL_SpikeUnit_ext)

os.chdir(ext_path)

# create a builder for the namespace
ns_builder.export(ns_path)



# test
load_namespaces(ns_path)

FLClusterMetricsTable = get_class('FLClusterMetricsTable', ns_name)
help(FLClusterMetricsTable)


FLSpikeEventSeries = get_class('FLSpikeEventSeries', ns_name)
help(FLSpikeEventSeries)
