#import the necessary classes from the pynwb package
from pynwb import NWBFile, get_class, load_namespaces, get_manager
from pynwb.ecephys import ElectricalSeries, EventWaveform, LFP, Device
from pynwb.behavior import BehavioralEpochs, BehavioralEvents, BehavioralTimeSeries, Position, CompassDirection, SpatialSeries
from pynwb.misc import IntervalSeries, UnitTimes, SpikeUnit
from pynwb.base import TimeSeries

from pynwb.form.backends.hdf5 import HDF5IO

import glob as glob
import argparse
import scipy.io as sio
import numpy as np
import os as os
import re as re
from datetime import datetime


from struct import unpack
from array import array


def loadmat(filename):
        '''
        this function should be called instead of direct sio.loadmat
        as it cures the problem of not properly recovering python dictionaries
        from mat files. It calls the function check keys to cure all entries
        which are still mat-objects
        '''
        data = sio.loadmat(filename, struct_as_record=False, squeeze_me=True)
        return _check_keys(data)

def loadmat2(filename):
        '''
        this function is the same as above but needs to be used when getting rid of empty entries doesn't work
        '''
        data = sio.loadmat(filename, struct_as_record=False, squeeze_me=False)
        return _check_keys(data)


def _check_keys(dict):
        '''
        checks if entries in dictionary are mat-objects. If yes
        todict is called to change them to nested dictionaries
        '''
        for key in dict:
                if isinstance(dict[key], sio.matlab.mio5_params.mat_struct):
                        dict[key] = _todict(dict[key])
        return dict

def _todict(matobj):
        '''
        A recursive function which constructs from matobjects nested dictionaries
        '''
        dict = {}
        for strg in matobj._fieldnames:
                elem = matobj.__dict__[strg]
                if isinstance(elem, sio.matlab.mio5_params.mat_struct):
                        dict[strg] = _todict(elem)
                elif isinstance(elem,np.ndarray):
                        dict[strg] = _tolist(elem)
                else:
                        dict[strg] = elem
        return dict

def _tolist(ndarray):
        '''
        A recursive function which constructs lists from cellarrays
        (which are loaded as numpy ndarrays), recursing into the elements
        if they contain matobjects.
        '''
        elem_list = []
        for sub_elem in ndarray:
                if isinstance(sub_elem, sio.matlab.mio5_params.mat_struct):
                        elem_list.append(_todict(sub_elem))
                elif isinstance(sub_elem,np.ndarray):
                        elem_list.append(_tolist(sub_elem))
                else:
                        elem_list.append(sub_elem)
        return elem_list


def get_files_by_day(base_dir, prefix, data_type):
        ''' Get files of a specific data types and return them in a dictionary indexed by day'''
        files = glob.glob("%s/%s%s*.mat" % (base_dir, prefix, data_type))
        f_re = re.compile('%s(?P<data_type>[a-z]+)(?P<day>[0-9]{2})' % prefix)
        ret = dict()
        for f in files:
                d = f_re.search(f)
                if d.group('data_type') != data_type:
                        continue
                day = int(d.group('day'))
                ret[day] = f

        return ret



def get_eeg_by_day(EEG_dir, data_type):
        '''
                Get eeg files in EEG dir and return separated by day and tetrode, and sorted by epoch

                Args:
                        EEG_dir: the path to the 'EEG' directory
        '''
        # can't do *eeg*.mat since that would get *eeggnd*.mat files as well
        files = glob.glob("%s/*.mat" % EEG_dir)
        prefix = os.path.commonprefix(files).split('/')[-1]
        fp_re = re.compile('%s(?P<data_type>[a-z]+)(?P<day>[0-9]{2})-(?P<epoch>[0-9])-(?P<tetrode>[0-9]{2})' % prefix)
        ret = dict()
        for f in files:
                d = fp_re.search(f)
                if d.group('data_type') != data_type:
                        continue
                day = int(d.group('day'))
                epoch = int(d.group('epoch'))
                tetrode = int(d.group('tetrode'))
                if day not in ret:
                        ret[day] = dict()
                if tetrode not in ret[day]:
                        ret[day][tetrode] = dict()
                ret[day][tetrode][epoch] = f
        for day in ret.keys():
                for tetrode in ret[day].keys():
                        sorted_list = list()
                        for epoch in sorted(ret[day][tetrode].keys()):
                                # create a sorted list of the epochs and store the epoch and tetrodes so we can index into the matlab class
                                # properly
                                sorted_list.append((ret[day][tetrode][epoch], (day, epoch, tetrode)))
                        ret[day][tetrode] = sorted_list
        return {'prefix': prefix, 'files': ret}

def parse_times(path):
        epoch_data = list()
        times = sio.loadmat(path)
        ranges = iter(times['ranges'])
        names = iter(times['names'])
        next(ranges)
        next(names)
        for name, time_range in zip(names, ranges):
                epoch_data.append({'description': name, 'start': ranges[0], 'stop': ranges[1]})
        return epoch_data


def build_day_eeg(files_by_tetrode, samprate):
        data = list()
        times = list()
        for file_info in files_by_tetrode:
                mat = loadmat2(file_info[0])
                (day, epoch, tet_num) = file_info[1]
                eegstruct = mat.get('eeg')[0][day-1][0][epoch-1][0][tet_num-1][0][0]
                # create a list of times for the data
                times.extend(eegstruct.starttime[0][0] + np.arange(0,len(eegstruct.data)-1) / samprate)
                data.extend(np.ndarray.tolist(eegstruct.data.flatten()))
        return np.array(times).T, np.array(data).T

def get_spikewaveform_files_by_day(raw_data_dir):
        # traverse the raw data directory and create a dictionary of waveform / timestamp files by day and tetrode
        files = dict()
        fp_re = re.compile('(?P<animal_name>[a-z]+)(?P<day>[0-9]{2})/(?P<tet_num>[0-9]{2})-(?P<depth>[0-9]{3})/.*mat')
        for dir_name, subdir_list, file_list in os.walk(raw_data_dir[0]):
                for file_name in file_list:
                        # check if the directory and file matches a waveform and timestamp file name
                        full_file_name = os.path.join(dir_name, file_name)
                        d = fp_re.search(full_file_name)
                        if d and d.group('animal_name'):
                                # the entire thing matched, so we parse the file name for the dictionary
                                day = int(d.group('day'))
                                tet_num = int(d.group('tet_num'))
                                # add dictionary entries if necessary
                                if day not in files.keys():
                                        files[day] = dict()
                                files[day][tet_num] = full_file_name
        return files


#load the Frank Lab extensions

ext_path = '/home/loren/Src/NWB/franklabnwb/fl_extensions'
ns_name = 'franklab'
ns_path = '%s/%s.namespace.yaml' % (ext_path, ns_name)
load_namespaces(ns_path)
# get the extension classes we need
#FL_ElectrodeTableRegion = get_class('FL_ElectrodeTableRegion', ns_name)
FL_SpikeUnit = get_class('FL_SpikeUnit', ns_name)
FL_ClusterQuality = get_class('FL_ClusterQuality', ns_name)



# Current plan
# All objects will have a starttime of 0 and will measure time in seconds since the beginning of Unix time (seconds since Jan 01 1970 UTC)
# Epoch object represents a day of recording
# BehavioralEpoch objects contain all behavioral epochs per track

dataset_zero_time = datetime(2006, 1, 1, 12, 0, 0) #NOTE: this is not the actual zero_time, as we don't have easy access to that.
zero_time = datetime(1970, 1, 1, 12, 0, 0)
create_date = datetime.now()
time_since_start = (dataset_zero_time - zero_time).total_seconds();

source = 'NSpike data acquisition system'
eeg_samprate = 1500.0

eeg_path = "EEG"
epochs_file = "times.mat"
tetinfo_file = "tetinfo.mat"
timestamps_per_sec = 10000
parser = argparse.ArgumentParser(usage="%(prog)s data_dir [--raw_dir raw_data_dir] out.nwb")
parser.add_argument('data_dir', type=str, help="directory of matlab data extracted from NSpike .dat files")
parser.add_argument('--raw_dir', nargs=1, type=str, action='store', help="location of base directory for raw data (e.g. /data/mkarlsson/bond)")
parser.add_argument('nwb_file', type=str, help="nwb file name")

args = parser.parse_args()
# check the input arguments
if not os.path.exists(args.data_dir):
        print('Error: data_dir %s does not exist' % args.data_dir)
        exit(-1)
if args.raw_dir and not os.path.exists(args.raw_dir[0]):
        print('Error: raw_dir %s does not exist' % args.raw_dir[0])
        exit(-1)



eeg_path = os.path.join(args.data_dir, eeg_path)

print(args.nwb_file)

data_source = 'Animal Bond'

nwbf = NWBFile(args.nwb_file,
                           'Converted data from %s' % args.data_dir,
                           data_source, datetime.now(), file_create_date=create_date,
                           lab='Frank Laboratory',
                           experimenter='Mattias Karlsson',
                           institution='UCSF',
                           experiment_description='Recordings from awake behaving rat',
                           session_id=args.data_dir)


# get LFP/EEG data from EEG subdir of preprocessing directory (i.e. 'Bon' directory)
prefix, eeg_files = map(get_eeg_by_day(eeg_path, 'eeg').get, ('prefix', 'files'))

#
# create a module for behavior
behav_mod = nwbf.create_processing_module("Behavior Module", data_source,'Behavioral Variables')
# create position, direction and speed
position_list = list()
pos = Position(data_source, position_list)
direction_list = list()
dir = CompassDirection(data_source, direction_list)
speed_list = list()
speed = BehavioralTimeSeries(data_source, speed_list)

# creat a list to store all of the fields in the task structure and a parallel list to store the created task interval structures


task_fields = list()
task_intervals = list()
# NOTE that day_inds is 0 based
day_inds = list()
time_list = dict()
nwb_epoch = dict()
pos_files = get_files_by_day(args.data_dir, prefix, 'pos')
task_files = get_files_by_day(args.data_dir, prefix, 'task')



for day in task_files.keys():
        day_inds.append(day)
        mat = loadmat(task_files[day])

        if (day  == 0):
                task_struct = mat.get('task')
        else:
                task_struct = mat.get('task')[day - 1]
        # find the pos file for this day and load it
        try:
                mat = loadmat(pos_files[day])
        except:
                print('Warning: no position file for day %d found in in %s' % (day, args.data_dir))
                continue
        if (day == 0):
                pos_struct = mat.get('pos')
        else:
                pos_struct = mat.get('pos')[day-1]

        # compile a list of time intervals in an array and create the position, head direction and velocity structures
        time_list[day] = list()
        time_index = 0

        #position_series = SpatialSeries('Position data for day %d' % day, 'overhead camera')
        for epoch_num, pos_epoch in enumerate(pos_struct):
                if pos_epoch != []:
                        # increment the time by time_since_start
                        pos_epoch.data[0:,0] += time_since_start
                        # if this has a data structure in it, add a list of times for the start and end, adding in time_since_start
                        time_list[day].append((pos_epoch.data[0,0], pos_epoch.data[-1,0]))
                        # we can also create new SpatialSeries for the position, direction and velocity information
                        #NOTE: Each new spatial series has to have a unique name.
                        pos.create_spatial_series('Position  day %d epoch %d' % (day, epoch_num), 'overhead camera',
                                                                          pos_epoch.data[1:2, 0:],
                                                                        '', conversion=pos_epoch.cmperpixel, timestamps = pos_epoch.data[0:,0])

                        dir_tmp = SpatialSeries('Head Direction  day %d epoch %d'% (day, epoch_num), 'overhead camera',
                                                                        pos_epoch.data[3,0:],
                                                                        '', timestamps = pos_epoch.data[0:,0])
                        dir.add_spatial_series(dir_tmp)

                        speed_tmp = TimeSeries('Speed day %d epoch %d' % (day, epoch_num),
                                                                   'overhead camera',
                                                                   pos_epoch.data[4,0:],
                                                                   'pixels/sec',
                                                                   conversion=pos_epoch.cmperpixel,
                                                                   timestamps = pos_epoch.data[0:,0],
                                                                   description = 'smoothed movement speed estimate')
                        speed.add_timeseries(speed_tmp)


        # each day will be defined as a single Epoch in NWB so we go through and get the first and last time from the
                # position data
        day_start = time_list[day][0][0]
        day_end = time_list[day][-1][-1]
        nwb_epoch[day] = nwbf.create_epoch('day %s' % day, data_source, day_start, day_end, [], 'day %s' % day)
        # add ignore intervals for the spaces between our epochs (it's not clear if this is necessary, but it won't hurt)
        # also, there's probably a more "python-ic" way to do this, but I don't know what it is 8-)
        if len(time_list) > 1:
                n = 1
                while n <= len(time_list):
                        #nwb_epoch.add_ignore_interval(time_list[day][n-1][1], time_list[day][n][0])
                        n += 1

        # now we go through the task structure and add a new interval series or an interval to an existing interval series
        # for each element in the task structure
        for epoch_num, task_epoch in enumerate(task_struct):
                if task_epoch != []:
                        for field_name in task_epoch._fieldnames:
                                if field_name not in task_fields:
                                        # add the field_name to the list and create a new IntervalSeries for it
                                        task_fields.append(field_name)
                                        tmp_array = np.ndarray(2);
                                        tmp_array = [time_list[day][epoch_num][0], time_list[day][epoch_num][1]]
                                        tmp_interval = IntervalSeries(field_name, 'matlab task structure')
                                        # add the interval for this epoch
                                        tmp_interval.add_interval(time_list[day][epoch_num][0], time_list[day][epoch_num][1])
                                        task_intervals.append(tmp_interval)
                                else:
                                        # add the interval to appropriate element of the list
                                        task_intervals[task_fields.index(field_name)].add_interval(time_list[day][epoch_num][0], time_list[day][epoch_num][1])


# add the position, direction and speed data to the module
behav_mod.add_container(pos)
behav_mod.add_container(dir)
behav_mod.add_container(speed)



# Now add the complete list of task intervals to the behav_mod module
for interval in task_intervals:
        behav_mod.add_container(BehavioralEpochs('task information', interval))

# create an electrophsysiology module
#elect_mod = nwbf.create_processing_module('Electrophysiology Module', data_source, 'Spikes and LFP ')

# get the list of matlab files for the raw data if the directory was specified
if args.raw_dir:
        spikewaveform_file = get_spikewaveform_files_by_day(args.raw_dir)


# Create the electrode table.
# The logic here is as follows:
#   Each individual recording channel gets its own ElectrodeGroup and its own entry in the ElectrodeTable
#   Each set of channels is denoted by an FL_ElectrodeTableRegion which has an entry for the ntrode number
#       that corresponds to this set of channels.
#

io = HDF5IO(args.nwb_file, manager=get_manager(), mode='w')
io.write(nwbf)
io.close()

# we first create the ElectrodeTable that all the electrodes will go into
nchan_per_tetrode = 4 #these files all contain tetrodes, so we assume four channels
tetinfo_file = "%s/%s%s" % (args.data_dir, prefix, tetinfo_file)
recording_device = Device('NSpike acquisition system', data_source)
electrode_group = dict()
electrode_table_region = dict()
electrode_list = dict()
impedence = np.nan
filtering = 'unknown - likely 600Hz-6KHz'
max_tet_num = 0;
mat = loadmat2(tetinfo_file)
# NOTE: day_ind is 0 based, as is cell; only day and tetrode number have the original numbering scheme
#define the electrode table for this dataset.  This will create  electrode table entries for each day

for day_ind, day_struct in enumerate(mat.get('tetinfo')[0]):
        if len(day_struct[0]):
                day = day_ind + 1
                electrode_group[day] = dict()
                electrode_table_region[day] = dict()
                # kenny's data has a day epoch tetrode structure but duplicates the info across epochs, so we can use the first
                # epoch for everything
                for tet_ind, tet_struct in enumerate(day_struct[0][0][0]):
                        #print('making electrode group for day %d, tet %d' % (day, tet_ind))
                        tet_num = tet_ind + 1
                        chan_num = 0 # this will hold an incrementing channel number for the entire day of data
                        # go through the list of fields
                        hemi = '?'
                        coord = [np.nan, np.nan, np.nan]
                        location = '?'
                        sub_area = '?'
                        full_description = ''
                        if len(tet_struct[0]):
                                if (tet_num > max_tet_num):
                                        max_tet_num = tet_num
                                tet = tet_struct[0][0]
                                field_names = tet._fieldnames
                                for field in tet._fieldnames:
                                        # add the field to a full description string
                                        full_description += '%s:%s, ' % (field, str(getattr(tet, field)))
                                        if field == 'area':
                                                area = tet.area[0]
                                        if field == 'subarea' and len(tet.subarea):
                                                sub_area = tet.subarea[0]
                                        elif field == 'depth':
                                                coord = [np.nan, np.nan, tet.depth[0][0][0][0] / 12 / 80 * 25.4] #convert to mm
                                        elif field == 'hemisphere':
                                                hemisphere = tet.hemisphere[0]
                                location = area + ' ' + sub_area
                                channel_location = [location, location, location, location]
                                channel_coordinates = [coord, coord, coord, coord]
                                electrode_name = "%02d-%02d" % (day, tet_num)
                                description = "tetrode {tet_num} located in {hemi} hemisphere on day {day}".format(tet_num=tet_num,
                                                                                                                                                                                                   hemi=hemisphere,
                                                                                                                                                                                                   day=day)

                                # we need to create an electrode group for this tetrode
                                electrode_group[day][tet_num] = nwbf.create_electrode_group(electrode_name,
                                                                                                                                                        data_source,
                                                                                                                                                        description,
                                                                                                                                                        location,
                                                                                                                                                        recording_device)
                                for i in range(nchan_per_tetrode):
                                        # now add an electrode
                                        nwbf.add_electrode(chan_num,
                                                                                coord[0],
                                                                                coord[1],
                                                                                coord[2],
                                                                                impedence,
                                                                                location,
                                                                                filtering,
                                                                                description,
                                                                                electrode_group[day][tet_num])
                                        chan_num = chan_num + 1
                                # now that we've created four entries, one for each channel of the tetrode, we create a new
                                # electrode table region for this tetrode and number it appropriately
                                table_region_description = 'ntrode %d region' % tet_num
                                table_region_name = '%d' % tet_num
                                electrode_table_region[day][tet_num] = nwbf.create_electrode_table_region(
                                                                                                                                                        list(range(chan_num-nchan_per_tetrode,
                                                                                                                                                                           chan_num+1)),
                                                                                                                                                                 table_region_description,
                                                                                                                                                                 table_region_name)

#LFP data
lfp_data = list()
lfp = LFP(data_source, lfp_data)
# read data from EEG/*eeg*.mat files and build TimeSeries object

print('processing LFP data, %d days to process' % len(eeg_files))
for day in sorted(eeg_files.keys()):
        dayfiles = eeg_files[day]
        for tet_num in dayfiles.keys():
                timestamps, data = build_day_eeg(dayfiles[tet_num], eeg_samprate)
                # convert the timestamps to Unix time:
                timestamps += time_since_start
                name = "{prefix}eeg{day}{tet}".format(prefix=prefix, day=day, tet=tet_num)

                #lfp_data.append(ElectricalSeries(name, source, data, electrode_table_region[day][tet_num], starttime=0,
                #                                                                rate=eeg_samprate, timestamps=timestamps))
                lfp.create_electrical_series(name, source, data, electrode_table_region[day][tet_num], starttime=0,
                                                                     rate=eeg_samprate, timestamps=timestamps)
        print('processed LFP data from day %d' % day)

#add the lfp data to the file
nwbf.add_acquisition(lfp)

#get the spike times from the spikes files
#each cluster gets a unique number starting at zero
cluster_num = 0
max_tet_num = 30
spike_files = get_files_by_day(args.data_dir, prefix, 'spikes')
spike_mod = nwbf.create_processing_module('Spike Data Module', data_source, 'Clustered Spikes')
# create an EventWaveform group to store the waveforms for this day of recording. We will add this EventWaveForm to
# spike_mod once we've populated it. Note that it requires that we put in a list for the SpikeEventSeries
spike_event_series = list()
event_waveform = EventWaveform(data_source, spike_event_series)


spike_unit = list()
valid_times = list()
for day in day_inds:
        try:
                # note that we need to not "squeeze" the matlab structure in this case, so we use loadmat2
                mat = loadmat2(spike_files[day])
        except:
                print('Warning: no spike file for day %d found in in %s' % (day, args.data_dir))
                continue

        spike_struct = mat.get('spikes')[0][day - 1]

        # go through all the spikes and rearrange the matlab structures by tetrode, cell number and epoch
        cell_by_tet = dict()
        for epoch_num, espikes in enumerate(spike_struct[0]):
                for tet_ind, tspikes in enumerate(espikes[0]):
                        tet_num = tet_ind + 1
                        if tet_num not in cell_by_tet:
                                # use tet_ind + 1 to keep the numbers consistent with the original data collection
                                cell_by_tet[tet_num] = dict()
                        for cell_num, cspikes in enumerate(tspikes[0]):
                                # check to see if there is something in the structure
                                if len(cspikes):
                                        if cell_num not in cell_by_tet[tet_num]:
                                                cell_by_tet[tet_num][cell_num] = dict()
                                        cell_by_tet[tet_num][cell_num][epoch_num] = cspikes[0][0]
        # now we create the SpikeEventStructures and their containing EventWaveform objects
        for tet_num in cell_by_tet.keys():
                for cell_num in cell_by_tet[tet_num].keys():
                        cluster_name = '%d' % cluster_num
                        cell_tmp = cell_by_tet[tet_num][cell_num]
                        # construct a full data array and a parallel list of valid times
                        spiketimes = list()
                        valid_times.append(IntervalSeries(cluster_name, 'spike structure'))
                        for epoch in cell_tmp:
                                if len(cell_tmp[epoch].data):
                                        spiketimes.extend(np.ndarray.tolist(cell_tmp[epoch].data[0:,0]))
                                        valid_times[cluster_num].add_interval(time_list[day][epoch_num][0], time_list[day][epoch_num][1])

                        # change the timestamps back to samples and look up the spike waveforms from the raw data if the raw
                        # data directory was specified
                        cluster_waveforms = []
                        if args.raw_dir:
                                timestamps = np.round(np.asarray(spiketimes) * timestamps_per_sec)
                                # open the corresponding matlab file for this day
                                mat = loadmat(spikewaveform_file[day][tet_num])
                                wave_timestamps = mat.get('timestamps')
                                waves = mat.get('waves')
                                index_list = np.searchsorted(wave_timestamps, timestamps)
                                # print('cluster %d, max index diff = %d' % (cluster_num, np.max(timestamps-wave_timestamps[index_list])))
                                # the cluster waveforms need to have a first dimension the same size as spiketimes, so we have to swap
                                # the 0th and 2nd dimension
                                cluster_waveforms = np.swapaxes(waves[:, :, index_list], 0, 2)
                        else:
                                cluster_waveforms = np.empty((len(spiketimes), nchan_per_tetrode, nsamp_per_spike)) * np.nan
                        #create and EventSeries for the spike waveforms

                        #test: add a new SpikeEventSeries for this cluster
                        event_waveform.create_spike_event_series('%d' % cluster_num,
                                                                                                        source,
                                                                                                        cluster_waveforms,
                                                                                                        np.asarray(spiketimes),
                                                                                                        electrode_table_region[day][tet_num],
                                                                                                        description = '%sspikes day %d tetrode %d cell %d data' % (
                                                                                                                                        prefix, day, tet_num, cell_num),
                                                                                                        conversion=0.000001)

                        # now add the SpikeUnit for this cluster, which is just a list of spike times
                        name = '%d' % cluster_num
                        # create an empty cluster_quality object
                        cluster_quality = FL_ClusterQuality(data_source, name, isolation_distance=np.nan, noise_overlap=np.nan)
                        spike_unit.append(FL_SpikeUnit(name, np.asarray(spiketimes), description, data_source,
                                                                                   event_waveform, valid_times[cluster_num],
                                                                                   electrode_table_region[day][tet_num],
                                                                                   cluster_quality))
                        # move on to the next cluster number
                        #if np.mod(cluster_num,10) == 0:
                        print('cluster num %d' % cluster_num)
                        cluster_num += 1

#                       break
#       if day > 2:
#               break

spike_mod.add_container(event_waveform)

# create the UnitTimes container for the list of all unit times and add the list of SpikeUnits to it
unit_times = UnitTimes(data_source, spike_unit, name='UnitTimes from' + description)

#spike_mod.add_container(unit_times)

# make an NWBFile
io = HDF5IO(args.nwb_file, manager=get_manager(), mode='w')
io.write(nwbf)
io.close()

io = HDF5IO(args.nwb_file, mode='r')
container = io.read()

